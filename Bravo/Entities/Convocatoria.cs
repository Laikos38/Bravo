﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bravo.Entities
{
    public class Convocatoria
    {
        public bool confirmo { get; set; }
        public DateTime fechaHora { get; set; }
        public DateTime fechaHoraRespuesta { get; set; }
    }
}
