﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bravo.Entities
{

    public class Dotacion
    {
        public Dotacion() { }

        public Dotacion(string kmAlBolber, DateTime fechaLlegada)
        {
            this.fechaHoraLlegada = fechaLlegada;
            this.kmUnidadAlVolver = kmAlBolber;
        }
        public DateTime fechaHoraSalida { get; set; }
        public UnidadMovil unidadMovil { get; set; }
        public DateTime fechaHoraLlegada { get; set; }
        public string kmUnidadAlVolver { get; set; }
        public string kmUnidadAlSalir { get; set; }
    }
}
