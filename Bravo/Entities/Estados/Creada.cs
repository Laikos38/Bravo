﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bravo.Entities.Estados
{
    class Creada : Estado
    {
        public Creada()
        {
            nombre = "Creada";
        }
        override
            public void Finalizar(Intervencion intervencion)
        { }
    }
}
