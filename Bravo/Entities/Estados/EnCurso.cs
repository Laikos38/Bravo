﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bravo.Entities.Estados;

namespace Bravo.Entities.Estados
{
    class EnCurso : Estado
    {
        public EnCurso()
        {
            nombre = "En curso";
        }

        override
        public void Finalizar(Intervencion interv)
        {
            Console.WriteLine("\n \n Entro al fen curso");

            this.ActualizarHistorial(interv);

            Finalizada finalizada = new Finalizada();
            interv.estadoActual = finalizada;

            Console.WriteLine(interv.ToString());

            this.RenovarHistorial(interv);
        }

        public void ActualizarHistorial(Intervencion interv)
        {
            interv.GetHistorialActual().fechaFin = DateTime.Now;          
        }

        public void RenovarHistorial(Intervencion interv)
        {
            HistorialIntervencion nuevoHist = new HistorialIntervencion(interv.estadoActual);
            interv.AgregarHistorial(nuevoHist);
        }
    }
}
