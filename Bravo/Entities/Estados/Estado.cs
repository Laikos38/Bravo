﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bravo.Entities.Estados
{
    public abstract class Estado
    {
        public string nombre { get; set; }
        public string ambito { get; set; }
        public string descripcion { get; set; }

        override
        public string ToString()
        {
            return this.nombre;
        }

        public abstract void Finalizar(Intervencion interv);

        public void ActualizarDatosDotaciones(Intervencion interv) { }
    }
}
