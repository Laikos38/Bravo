﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bravo.Entities.Estados
{
    class Finalizada : Estado
    {
        public Finalizada()
        {
            nombre = "Finalizada";
        }

        public void ActualizarDatosDotaciones(Intervencion interv)
        {

        }

        override
            public void Finalizar(Intervencion intervencion)
        { }
    }
}
