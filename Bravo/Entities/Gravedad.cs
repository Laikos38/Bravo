﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bravo.Entities
{
    public class Gravedad
    {
        public string nombre { get; set; }
        public string descripcion { get; set; }

        override
        public string ToString()
        {
            return this.nombre;
        }
    }
}
