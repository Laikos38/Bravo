﻿using Bravo.Entities.Estados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bravo.Entities
{
    public class HistorialIntervencion
    {
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public Estado estado { get; set; }

        public HistorialIntervencion()
        {
        }

        public HistorialIntervencion(Estado e)
        {
            estado = e;
        }

        override
        public string ToString()
        {
            string rtn;
            rtn = "Estado: " + estado.ToString() + "\n" +
                  "Fecha Inicio: " + fechaInicio.ToString() + "\n" +
                  "Fecha Fin: " + fechaFin.ToString() + "\n";
            return rtn;
        }
    }
}
