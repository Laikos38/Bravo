﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bravo.Entities.Estados;

namespace Bravo.Entities
{
    public class Intervencion
    {
        public int codIntervencion { get; set; }
        public string domicilioReportado { get; set; }
        public string nombreApellidoInformante { get; set; }
        public string resumenSiniestroInformante { get; set; }
        public string resumenTrabajoEfectuado { get; set; }
        public string telefonoContacto { get; set; }
        public Bombero encargado { get; set; }
        public Estado estadoActual { get; set; }
        public List<HistorialIntervencion> historial { get; set; }
        public TipoSiniestro tipoSiniestro { get; set; }
        public List<Material> materialesUtilizados { get; set; }
        public List<Convocatoria> convocatoria { get; set; }
        public List<Dotacion> dotacion { get; set; }
        public Gravedad gravedad { get; set; }
        public DateTime fechaCreacion { get; set; }


        public void AgregarHistorial(HistorialIntervencion historial_asdasdasd)
        {
            this.historial.Add(historial_asdasdasd);
        }

        public void AgregarDotacion(Dotacion dot)
        {
            this.dotacion.Add(dot);
        }

        override
        public string ToString()
        {
            string rtn;
            rtn = "Codigo: " + this.codIntervencion + "\n" +
                  "Gravedad: " + this.gravedad.ToString() + "\n" +
                  "Domicilio: " + domicilioReportado + "\n" +
                  "Estado Actual: " + this.estadoActual.ToString() + "\n" +
                  "Resumen: " + resumenSiniestroInformante + "\n" +
                  "KMs: " + dotacion[0].kmUnidadAlVolver +
                  "Historial Estados: \n";
            for(int i=0; i<this.historial.Count; i++)
            {
                this.historial[i].ToString();
            }
            return rtn;
        }

        public HistorialIntervencion GetHistorialActual()
        {
            foreach (HistorialIntervencion hist in historial)
            {
                if (hist.fechaFin == default(DateTime)) return hist;
            }
            return null;
        }

        public void finalizarIntervencion()
        {
            Console.WriteLine("\n" + this.ToString());

            this.estadoActual.Finalizar(this);

            this.estadoActual.ActualizarDatosDotaciones(this);
        }
    }
}
