﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bravo.Entities
{
    public class UnidadMovil
    {
        public string kilometraje { get; set; }
        public string nroUnidad { get; set; }

        override
        public string ToString()
        {
            return nroUnidad;
        }
    }
}
