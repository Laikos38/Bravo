﻿using Bravo.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bravo.GUILayer
{
    public partial class frmDatosDotacion : Form
    {
        private Dotacion d;

        public frmDatosDotacion(Dotacion d)
        {
            this.d = d;
            InitializeComponent();
            txtFechaHoraSalida.Text = d.fechaHoraSalida.ToString();
            txtFechaHoraSalida.Enabled = false;
            txtUnidadMovil.Text = d.unidadMovil.nroUnidad;
            txtUnidadMovil.Enabled = false;
            txtKmSalida.Text = d.kmUnidadAlSalir;
            txtKmSalida.Enabled = false;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            this.d.fechaHoraLlegada = dtpFechaHoraLlegada.Value;
            this.d.kmUnidadAlVolver = txtKmLlegada.Text;
            frmDotacionesIntervencion frmPadre = this.Owner as frmDotacionesIntervencion;
            frmPadre.TomarDatos(d);
            this.Close();
        }

        private void TxtKmLlegada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar)) //Al pulsar un número
            {
                e.Handled = false; //Se acepta
            }
            else if (Char.IsControl(e.KeyChar)) //Teclas especial como borrar
            {
                e.Handled = false; //Se acepta
            }
            else //Para todas las demas teclas
            {
                e.Handled = true; //No se acepta
            }
        }

        private void frmDatosDotacion_Load(object sender, EventArgs e)
        {

        }
    }
}
