﻿using Bravo.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bravo.GUILayer
{
    public partial class frmDotacionesIntervencion : Form
    {
        private Intervencion intervSeleccionada;
        private Dotacion dotacionSeleccionada;
        private int indiceActual;
        private GestorFinalizarIntervencion gestor;
        public frmDotacionesIntervencion(Intervencion interv, GestorFinalizarIntervencion gestorsito)
        {
            InitializeComponent();
            intervSeleccionada = interv;
            InitializeDataGridView();
            btnDetalleDotacion.Enabled = false ;
            this.gestor = gestorsito;
        }

        private void InitializeDataGridView()
        {
            //Cantidad columnas y las hago visibles.
            dgvDotaciones.ColumnCount = 5;
            dgvDotaciones.ColumnHeadersVisible = true;

            // Configurp la AutoGenerateColumns en false para que no se autogeneren las columnas
            dgvDotaciones.AutoGenerateColumns = false;

            // Definimos el nombre de la columnas y el DataPropertyName que se asocia a DataSource
            dgvDotaciones.Columns[0].Name = "Fecha hora salida";
            dgvDotaciones.Columns[0].DataPropertyName = "fechaHoraSalida";

            dgvDotaciones.Columns[1].Name = "Unidad Móvil";
            dgvDotaciones.Columns[1].DataPropertyName = "unidadMovil";

            dgvDotaciones.Columns[2].Name = "Km al salir";
            dgvDotaciones.Columns[2].DataPropertyName = "kmUnidadAlSalir";

            dgvDotaciones.Columns[3].Name = "Fecha hora llegada";
            dgvDotaciones.Columns[3].DataPropertyName = "fechaHoraLlegada";

            dgvDotaciones.Columns[4].Name = "Km al volver";
            dgvDotaciones.Columns[4].DataPropertyName = "kmUnidadAlVolver";

            // Cambia el tamaño de la altura de los encabezados de columna.
            dgvDotaciones.AutoResizeColumnHeadersHeight();

            // Cambia el tamaño de todas las alturas de fila para ajustar el contenido de todas las celdas que no sean de encabezado.
            dgvDotaciones.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders);

            this.dgvDotaciones.MultiSelect = false;
            this.dgvDotaciones.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvDotaciones.AllowUserToAddRows = false;
        }

        private void FrmDotacionesIntervencion_Load(object sender, EventArgs e)
        {
            dgvDotaciones.DataSource = intervSeleccionada.dotacion;
        }

        private void BtnDetalleDotacion_Click(object sender, EventArgs e)
        {
            if (dgvDotaciones.SelectedRows == null)
            {
                MessageBox.Show("¡Debe seleccionar una dotación!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                indiceActual = dgvDotaciones.CurrentRow.Index;
                Dotacion d = (Dotacion)dgvDotaciones.CurrentRow.DataBoundItem;

                frmDatosDotacion frmDD = new frmDatosDotacion(d);
                frmDD.FormClosing += FrmDatosDotaciones_Close;
                AddOwnedForm(frmDD);
                this.Hide();
                frmDD.Show();
            }
        }

        private void FrmDatosDotaciones_Close(object sender, EventArgs e)
        {
            this.Show();
            
        }

        private void DgvDotaciones_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnDetalleDotacion.Enabled = true;
        }

        public void TomarDatos(Dotacion d)
        {
            this.dotacionSeleccionada = d;
            intervSeleccionada.dotacion.RemoveAt(indiceActual);
            intervSeleccionada.dotacion.Add(d);
            dgvDotaciones.DataSource = intervSeleccionada.dotacion;
        }

        private void panelFinalizarInterv_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            gestor.finalizarIntervencion(intervSeleccionada);
            MessageBox.Show("Intervención finalizada con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void dgvDotaciones_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
