﻿using Bravo.Entities;
using Bravo.Entities.Estados;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Bravo.GUILayer
{
    public partial class frmFinalizarIntervencion : Form
    {
        private Intervencion oInterv = new Intervencion();
        private GestorFinalizarIntervencion gestor = new GestorFinalizarIntervencion(); 

        public frmFinalizarIntervencion()
        {
            InitializeComponent();
            InitializeDataGridView();
        }

        private void InitializeDataGridView()
        {
            //Cantidad columnas y las hago visibles.
            dgvIntervenciones.ColumnCount = 3;
            dgvIntervenciones.ColumnHeadersVisible = true;

            // Configurp la AutoGenerateColumns en false para que no se autogeneren las columnas
            dgvIntervenciones.AutoGenerateColumns = false;

            // Definimos el nombre de la columnas y el DataPropertyName que se asocia a DataSource
            dgvIntervenciones.Columns[0].Name = "Fecha creación";
            dgvIntervenciones.Columns[0].DataPropertyName = "fechaCreacion";

            dgvIntervenciones.Columns[1].Name = "Ubicación";
            dgvIntervenciones.Columns[1].DataPropertyName = "domicilioReportado";

            dgvIntervenciones.Columns[2].Name = "Resumen";
            dgvIntervenciones.Columns[2].DataPropertyName = "resumenSiniestroInformante";

            // Cambia el tamaño de la altura de los encabezados de columna.
            dgvIntervenciones.AutoResizeColumnHeadersHeight();

            // Cambia el tamaño de todas las alturas de fila para ajustar el contenido de todas las celdas que no sean de encabezado.
            dgvIntervenciones.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders);

            this.dgvIntervenciones.MultiSelect = false;
            this.dgvIntervenciones.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvIntervenciones.AllowUserToAddRows = false;
        }

        private void FrmFinalizarIntervencion_Load(object sender, EventArgs e)
        {
            GestorFinalizarIntervencion gestor = new GestorFinalizarIntervencion();
            this.btnFinalizar.Enabled = false;

            dgvIntervenciones.DataSource = gestor.tomarListaIntervActivas();

        }

        private void DgvIntervenciones_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.btnFinalizar.Enabled = true;
        }

        private void BtnFinalizar_Click(object sender, EventArgs e)
        {
            if (dgvIntervenciones.SelectedRows == null)
            {
                MessageBox.Show("¡Debe seleccionar una intervención!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                int idx = dgvIntervenciones.CurrentRow.Index;

                Intervencion oInterv = gestor.buscarInterv(idx);
                frmDotacionesIntervencion frmDI = new frmDotacionesIntervencion(oInterv, gestor);
                frmDI.FormClosing += FrmDotaciones_Close;
                this.Hide();
                frmDI.Show();
             
            }
        }

        private void FrmDotaciones_Close(object sender, EventArgs e)
        {
            this.Show();
            Console.WriteLine("aaa\n\n");
            this.dgvIntervenciones.DataSource = gestor.buscarIntervencionesActivas();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvIntervenciones_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panelFinalizarInterv_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
