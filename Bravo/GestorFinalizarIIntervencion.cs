﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bravo.Entities;
using Bravo.Entities.Estados;

namespace Bravo
{
    public class GestorFinalizarIntervencion
    {
        List<Intervencion> intervenciones = new List<Intervencion>();
        Intervencion selIntervencion;

        public GestorFinalizarIntervencion()
        {
            Intervencion oInterv = new Intervencion();
            // Creando intervencion a finalizar
            oInterv.codIntervencion = 3124;
            oInterv.domicilioReportado = "Estrada 357 3ro C";
            oInterv.resumenSiniestroInformante = "Incendio copado.";
            DateTime date_0 = new DateTime(2019, 5, 1, 8, 30, 52);
            oInterv.fechaCreacion = date_0;
            Gravedad oGravedad = new Gravedad();
            oGravedad.nombre = "Baja";
            oInterv.gravedad = oGravedad;
            List<HistorialIntervencion> h = new List<HistorialIntervencion>();
            oInterv.historial = h;


            HistorialIntervencion historial_1 = new HistorialIntervencion();
            DateTime date_1 = new DateTime(2019, 5, 1, 8, 30, 52);
            DateTime date_2 = new DateTime(2019, 5, 1, 9, 30, 0);
            Creada oCreada = new Creada();
            historial_1.estado = oCreada;
            historial_1.fechaInicio = date_1;
            historial_1.fechaInicio = date_2;
            oInterv.AgregarHistorial(historial_1);

            HistorialIntervencion historial_2 = new HistorialIntervencion();
            date_1 = new DateTime(2019, 5, 1, 9, 30, 0);
            date_2 = new DateTime(2019, 5, 1, 10, 35, 10);
            Programada oProgramada = new Programada();
            historial_2.estado = oProgramada;
            historial_2.fechaInicio = date_1;
            historial_2.fechaInicio = date_2;
            oInterv.AgregarHistorial(historial_2);

            HistorialIntervencion historial_3 = new HistorialIntervencion();
            date_1 = new DateTime(2019, 5, 1, 9, 30, 0);
            date_2 = new DateTime(2019, 6, 1, 11, 10, 32);
            Convocada oConvocada = new Convocada();
            historial_3.estado = oConvocada;
            historial_3.fechaInicio = date_1;
            historial_3.fechaInicio = date_2;
            oInterv.AgregarHistorial(historial_3);

            HistorialIntervencion historial_4 = new HistorialIntervencion();
            date_1 = new DateTime(2019, 5, 1, 9, 30, 0);
            date_2 = new DateTime(2019, 6, 1, 12, 17, 15);
            EnCurso oEnCurso = new EnCurso();
            historial_4.estado = oEnCurso;
            historial_4.fechaInicio = date_1;
            historial_4.fechaInicio = date_2;
            oInterv.AgregarHistorial(historial_4);

            oInterv.estadoActual = oEnCurso;

            List<Dotacion> d = new List<Dotacion>();
            oInterv.dotacion = d;

            Dotacion dot1 = new Dotacion();
            dot1.fechaHoraSalida = date_1;
            UnidadMovil unidad1 = new UnidadMovil();
            unidad1.nroUnidad = "9";
            dot1.unidadMovil = unidad1;
            dot1.kmUnidadAlSalir = "104029";

            Dotacion dot2 = new Dotacion();
            dot2.fechaHoraSalida = date_1;
            UnidadMovil unidad2 = new UnidadMovil();
            unidad2.nroUnidad = "12";
            dot2.unidadMovil = unidad2;
            dot2.kmUnidadAlSalir = "9124";

            oInterv.AgregarDotacion(dot1);
            oInterv.AgregarDotacion(dot2);




            Intervencion oInterv1 = new Intervencion();
            // Creando intervencion a finalizar
            oInterv1.codIntervencion = 3124;
            oInterv1.domicilioReportado = "Estrada 357 3ro C";
            oInterv1.resumenSiniestroInformante = "Incendio domiciliario.";
            DateTime date_11 = new DateTime(2019, 5, 1, 8, 30, 52);
            oInterv1.fechaCreacion = date_0;
            Gravedad oGravedad1 = new Gravedad();
            oGravedad.nombre = "Baja";
            oInterv1.gravedad = oGravedad;
            List<HistorialIntervencion> h1 = new List<HistorialIntervencion>();
            oInterv1.historial = h;


            HistorialIntervencion historial_11 = new HistorialIntervencion();
            DateTime date_111 = new DateTime(2019, 5, 1, 8, 30, 52);
            DateTime date_21 = new DateTime(2019, 5, 1, 9, 30, 0);
            Creada oCreada1 = new Creada();
            historial_1.estado = oCreada;
            historial_1.fechaInicio = date_1;
            historial_1.fechaInicio = date_2;
            oInterv1.AgregarHistorial(historial_1);

            HistorialIntervencion historial_21 = new HistorialIntervencion();
            date_1 = new DateTime(2019, 5, 1, 9, 30, 0);
            date_2 = new DateTime(2019, 5, 1, 10, 35, 10);
            Programada oProgramada1 = new Programada();
            historial_2.estado = oProgramada;
            historial_2.fechaInicio = date_1;
            historial_2.fechaInicio = date_2;
            oInterv1.AgregarHistorial(historial_2);

            HistorialIntervencion historial_31 = new HistorialIntervencion();
            date_1 = new DateTime(2019, 5, 1, 9, 30, 0);
            date_2 = new DateTime(2019, 6, 1, 11, 10, 32);
            Convocada oConvocada1 = new Convocada();
            historial_3.estado = oConvocada;
            historial_3.fechaInicio = date_1;
            historial_3.fechaInicio = date_2;
            oInterv1.AgregarHistorial(historial_3);

            HistorialIntervencion historial_41 = new HistorialIntervencion();
            date_1 = new DateTime(2019, 5, 1, 9, 30, 0);
            date_2 = new DateTime(2019, 6, 1, 12, 17, 15);
            EnCurso oEnCurso1 = new EnCurso();
            historial_4.estado = oEnCurso;
            historial_4.fechaInicio = date_1;
            historial_4.fechaInicio = date_2;
            oInterv1.AgregarHistorial(historial_4);

            oInterv1.estadoActual = oEnCurso;

            List<Dotacion> d1 = new List<Dotacion>();
            oInterv1.dotacion = d1;

            Dotacion dot11 = new Dotacion();
            dot11.fechaHoraSalida = date_1;
            UnidadMovil unidad11 = new UnidadMovil();
            unidad11.nroUnidad = "9";
            dot11.unidadMovil = unidad11;
            dot11.kmUnidadAlSalir = "104029";

            Dotacion dot21 = new Dotacion();
            dot21.fechaHoraSalida = date_1;
            UnidadMovil unidad21 = new UnidadMovil();
            unidad21.nroUnidad = "12";
            dot21.unidadMovil = unidad21;
            dot21.kmUnidadAlSalir = "9124";

            oInterv1.AgregarDotacion(dot11);
            oInterv1.AgregarDotacion(dot21);

            this.intervenciones.Add(oInterv);
            this.intervenciones.Add(oInterv1);
        }

        public Intervencion Intervencion { get => selIntervencion; set => selIntervencion = value; }

        public void finalizarIntervencion(Intervencion interv)
        {
            interv.finalizarIntervencion();

        }

        public List<Intervencion> buscarIntervencionesActivas()
        {
            List<Intervencion> intervActivas = new List<Intervencion>();

            foreach (Intervencion intervencion in intervenciones)
            {
                if (intervencion.estadoActual.nombre == "En curso")
                {
                intervActivas.Add(intervencion);
                }
            }
            return intervActivas;
        }

        public Intervencion buscarInterv(int idx)
        {
            return this.intervenciones[idx];
        }

        public List<Intervencion> tomarListaIntervActivas()
        {
            return this.buscarIntervencionesActivas();
        }
    }
}
